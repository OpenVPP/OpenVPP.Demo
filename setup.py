from setuptools import setup  # , find_packages
import os

NAMESPACE = 'openvpp'
PACKAGE = 'demo'

setup(
    author='Martin Tröschel',
    author_email='martin.troeschel@gmail.com',
    description='A mosaik scenario combining OpenVPP.ISAAC and OpenVPP.ChpSim.',
    entry_points={
        'console_scripts': [
            'openvpp-run = openvpp.demo.run:main',
            'openvpp-scenario = openvpp.demo.scenario:main',
        ],
    },
    include_package_data=True,
    install_requires=[
        'aiomas[mpb]>=1.0.1',
        'arrow>=0.4',
        'click>=4.0',
        'h5py>=2.5',
        'numpy>=1.8',
        'psutil>=2.2',
    ],
    long_description=open('README.md').read(),
    #   + '\n\n' +
    #   open('CHANGES.txt').read() + '\n\n' +
    #   open('AUTHORS.txt').read(),
    name=PACKAGE,
    namespace_packages=[
        NAMESPACE,
        # 'openvpp.chpsim',
    ],
    # packages=find_packages(where='openvpp_isaac'),
    packages=[NAMESPACE, NAMESPACE + os.path.sep + PACKAGE],
    package_dir={'': '.'},
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    url='https://gitlab.com/OpenVPP/OpenVPP.demo',
    version='0.1.0',
)

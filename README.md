# OpenVPP.mosaik

A mosaik scenario combining OpenVPP.ISAAC and OpenVPP.ChpSim.

## Installing Python

Under Ubuntu:

    sudo apt install python3.4-dev python3.5-dev python3.6-dev

## Creating A Virtual Environment

    virtualenv -p python3.6 venv

## Activating the Virtual Environment

Under BASH:

    source venv/bin/activate

Under Windows:

    venv\Scripts\activate

## Installing Requirements

    pip install -r requirements.txt

and either

    pip install -r requirements_local.txt

or

    pip install -r requirements_gitlab.txt

## Simulating

    cd openvpp/demo
    python run.py

## Deactivating the Virtual Environment

    deactivate
